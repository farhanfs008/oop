<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new animal("shaun");
echo "Nama Hewan: $sheep->name<br>";
echo "Jumlah Kaki: $sheep->legs<br>";
echo "Darah dingin: $sheep->cold_blooded<br><br>";

$sungokong = new ape("kera sakti");
echo "Nama Hewan: $sungokong->name<br>";
echo "Jumlah Kaki: $sungokong->legs<br>";
echo "Darah dingin: $sungokong->cold_blooded<br>";
echo $sungokong->yell();
echo "<br><br>";

$kodok = new frog("buduk");
echo "Nama Hewan: $kodok->name<br>";
echo "Jumlah Kaki: $kodok->legs<br>";
echo "Darah dingin: $kodok->cold_blooded<br>";
echo $kodok->jump() ;